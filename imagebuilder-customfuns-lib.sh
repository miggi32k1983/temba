git_apply_patch() {
  local patch_var="${1}"
  local patch_file="${2}"
  local patch_path="${3}"

  # positionate on git root dir -> src https://stackoverflow.com/questions/957928/is-there-a-way-to-get-the-git-root-directory-in-one-command/957978#957978

  # undefined variable, bypass
  if [ -z ${patch_var} ]; then
    return 0
  fi

  # idempotent git apply -> src https://foss.heptapod.net/heptapod/omnibus/commit/b4a5f6d2f043d14e3fc9457e070e871e3f51e9ef
  if [ "${patch_var}" = 'y' ]; then
    echo "git applying ${patch_path}/${patch_file}"
    git apply "${patch_path}/${patch_file}" 2>/dev/null || (git apply -R "${patch_path}/${patch_file}" && git apply "${patch_path}/${patch_file}")
  else
    git apply -R "${patch_path}/${patch_file}" 2>/dev/null || true
  fi
}

copy_patch() {
  local patch_var="${1}"
  local target_path="${2}"
  local patch_file="${3}"
  #optional
  local patch_path="${4}"
  #default
  if [ -z "$patch_path" ]; then
    local patch_path="../patches"
  fi

  if [ -z ${patch_var} ]; then
    return 0
  fi

  if [ ${patch_var} = 'y' ]; then
    mkdir -p "${target_path}"
    cp -v "${patch_path}/${patch_file}" "${target_path}"
  else
    rm -fv "${target_path}/${patch_file}"
  fi
}
