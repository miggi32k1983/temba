origin of patches

- 999-compliance-test.patch thanks to https://github.com/libremesh/lime-sdk/blob/develop/snippets/regdbtz.sh
- 999-fix_bmx6info.patch small fix to avoid bmx6 error in logread https://github.com/openwrt-routing/packages/pull/579
- bmx6/Makefile get latest version of bmx6, right now this is in snapshot but not in 19.07.3 https://github.com/openwrt-routing/packages/pull/574
- 999-netifd-egress-ingress.patch is a merge candidate to openwrt master https://github.com/pespin/netifd/commit/ddec0628d4ba3f99f53cc2abd4cca6da8c82a1f1
- generic-ubnt.mk_ubntversion.patch is a merge candidate to openwrt master https://github.com/openwrt/openwrt/pull/3063 https://git.openwrt.org/?p=openwrt/staging/adrian.git;a=patch;h=b7953e2fa664dd2fb142d4855ed2501d47236cc6
- generic-ubnt.mk_ath10k.patch changes from usage of ath10k-ct to original ath10k firmware, which works better for our situation
- mt7621.mk_phicomm_k2p.patch backport wifi parameters that gives wifi phicomm taken from openwrt master commit 7d684b7673a7cb4f3fef366dd4f621f62fc4019c
- mt7621.mk_xiaomi_mi_4a.patch is the original commit to add support for xiaomi mi router 4a giga version https://git.openwrt.org/?p=openwrt/openwrt.git;a=commit;h=522d5ff42835f7ad01c090b9ea1863f5d56b91d3
